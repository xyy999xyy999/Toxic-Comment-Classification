import pandas as pd
import numpy as np


A = pd.read_csv("0.9866.csv")
B = pd.read_csv("0.9867.csv")
C = pd.read_csv("0.9867_shen.csv")
D = pd.read_csv("0.9868.csv")
E = pd.read_csv("0.9869.csv")
F = pd.read_csv("0.9869_2.csv")
# E = pd.read_csv("data/Blend/OOF_stacking_regime.csv")
# F = pd.read_csv("data/Blend/TestCNN.csv")
# G = pd.read_csv("data/Blend/Toxic_Avenger.csv")
# H = pd.read_csv("data/Blend/blend_it_all.csv")
#
Sum=66+67+67+68+69+69
c1=66/Sum
c2=67/Sum
c3=67/Sum
c4=68/Sum
c5=69/Sum
c6=69/Sum

b1 = A.copy()
col = A.columns

col = col.tolist()
col.remove('id')
for i in col:
    b1[i] = (c1 * A[i] + c2 * B[i] + c3 * C[i] + c4 * D[i] + c5 * E[i] + c6 * F[i]) / (c1+c2+c3+c4+c5+c6)

b1.to_csv('Result_blend.csv', index=False)